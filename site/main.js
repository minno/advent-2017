// The prefix of every sidebar tab element's ID.
const sidebarTabId = "sidebarTab";

// The index of the currently-selected tab.
let currSelected = null;

// Called when a sidebar tab is clicked on.
function tabClickHandler(event) {
    let tabEl = this;
    let index = +tabEl.id.substr(sidebarTabId.length);
    if (currSelected != null && currSelected != index) {
        let prevTabEl = document.getElementById(sidebarTabId + currSelected);
        prevTabEl.classList.remove("selected");
    }
    tabEl.classList.add("selected");
    currSelected = index;
    
    document.getElementById("adventPage").src = "http://adventofcode.com/2017/day/" + index;
    document.getElementById("input").value = "";
    document.getElementById("output").value = "";
    document.getElementById("solver").style.display = "";
}

// Called when the "=>" button is clicked.
function solveClickHandler(event) {
    let day = currSelected;
    let input = document.getElementById("input").value;
    
    run_day(day, input).then(output => {
        let outputBox = document.getElementById("output");
        outputBox.value = output;
    });
}

// Called when the refresh button is clicked.
function refreshHandler(_event) {
    reload_wasm();
}

// Actions to perform once the dom is available.
function init() {
    for (let i = 1; i <= 25; ++i) {
        let tabEl = document.getElementById(sidebarTabId + i);
        tabEl.onclick = tabClickHandler;
    }
    
    document.getElementById("solveButton").onclick = solveClickHandler;
    document.getElementById("refresh").onclick = refreshHandler;
}

window.onload = init;