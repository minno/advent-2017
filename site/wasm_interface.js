// Returns a promise providing access to the wasm module at the given URL.
function init_wasm(url) {
    return fetch(url).then(response => response.arrayBuffer())
        .then(bytes => WebAssembly.instantiate(bytes, {}))
        .then(results => results.instance);
}

// Allocates a wasm-accessible copy of the given string
function write_wasm_string(module, str) {
    let encoder = new TextEncoder("UTF-8");
    let string_buffer = encoder.encode(str);
    let ptr = module.exports.string_create(string_buffer.length);
    let str_data = module.exports.string_data(ptr);
    let str_len = module.exports.string_len(ptr);
    let mem = new Uint8Array(module.exports.memory.buffer, str_data, str_len);
    
    for (let i = 0; i < str_len; ++i) {
        mem[i] = string_buffer[i];
    }
    
    return ptr;
}

// Converts the given wasm-accessible string into a javascript string and deallocates it
function read_wasm_string(module, ptr) {
    let str_len = module.exports.string_len(ptr);
    let str_data = module.exports.string_data(ptr);
    let mem = new Uint8Array(module.exports.memory.buffer, str_data, str_len);
    
    let decoder = new TextDecoder("UTF-8");
    let str = decoder.decode(mem);
    module.exports.string_dealloc(ptr);
    return str;
}

// Might as well kick off the async load right away.
let module;
function reload_wasm() {
    module = init_wasm("advent_2017.wasm");
}
reload_wasm();

// Returns a promise containing the result from that day's problem
function run_day(day, input) {
    return module.then(module => {
        let input_wasm = write_wasm_string(module, input);
        let output_wasm = module.exports.get_answer(day, input_wasm);
        let output = read_wasm_string(module, output_wasm);
        module.exports.string_dealloc(input_wasm);
        return output;
    });
}