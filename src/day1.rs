pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn solve1(input: &str) -> String {
    common(input, 1)
}

fn solve2(input: &str) -> String {
    common(input, input.len()/2)
}

fn common(input: &str, skip_amt: usize) -> String {
    let input = input.trim();
    let pairs = {
        let mut firsts = input.chars().cycle();
        let mut seconds = input.chars().cycle();
        firsts.zip(seconds.skip(skip_amt)).take(input.len())
    };
    
    pairs.filter_map(
        |(ch1, ch2)| 
            if ch1 == ch2 { 
                ch1.to_digit(10) 
            } else { 
                None 
            }
    ).sum::<u32>().to_string()
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert_eq!(solve1("1122"), "3");
        assert_eq!(solve1("1111"), "4");
        assert_eq!(solve1("1234"), "0");
        assert_eq!(solve1("91212129"), "9");
    }
    
    #[test]
    fn test2() {
        assert_eq!(solve2("1212"), "6");
        assert_eq!(solve2("1221"), "0");
        assert_eq!(solve2("123425"), "4");
        assert_eq!(solve2("123123"), "12");
        assert_eq!(solve2("12131415"), "4");
    }
}