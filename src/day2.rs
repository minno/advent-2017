pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn solve1(input: &str) -> String {
    common(input, diff_checksum)
}

fn solve2(input: &str) -> String {
    common(input, div_checksum)
}

fn common(input: &str, checksum: fn(&[i32]) -> i32) -> String {
    input
        .trim()
        .split('\n')
        .map(|row| row.split_whitespace().flat_map(str::parse::<i32>).collect::<Vec<_>>())
        .map(|row| checksum(&row))
        .sum::<i32>()
        .to_string()
}

fn diff_checksum(row: &[i32]) -> i32 {
    row.iter().max().unwrap() - row.iter().min().unwrap()
}

fn div_checksum(row: &[i32]) -> i32 {
    for &num1 in row {
        for &num2 in row {
            if num1 != num2 && num1 % num2 == 0 {
                return num1 / num2;
            }
        }
    }
    -1
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert_eq!(diff_checksum(&[5, 1, 9, 5]), 8);
        assert_eq!(diff_checksum(&[7, 5, 3]), 4);
        assert_eq!(diff_checksum(&[2, 4, 6, 8]), 6);
        assert_eq!(solve1("5 1 9 5\n7 5 3\n2 4 6 8\n"), "18");
    }
    
    #[test]
    fn test2() {
        assert_eq!(div_checksum(&[5, 9, 2, 8]), 4);
        assert_eq!(div_checksum(&[9, 4, 7, 3]), 3);
        assert_eq!(div_checksum(&[3, 8, 6, 5]), 2);
        assert_eq!(solve2("5 9 2 8\n9 4 7 3\n3 8 6 5\n"), "9");
    }
}