pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn solve1(input: &str) -> String {
    let idx = input.trim().parse::<i32>().unwrap();
    let coords = coords(idx);
    (coords.0.abs() + coords.1.abs()).to_string()
}

fn solve2(input: &str) -> String {
    let input = input.trim().parse::<i32>().unwrap();
    let mut values = Vec::<((i32, i32), i32)>::new();
    values.push(((0, 0), 1));
    for i in 1.. {
        let idx = i+1;
        let coords = coords(idx);
        let mut value = 0;
        for &(prev_coords, prev_val) in &values {
            if is_adjacent(prev_coords, coords) {
                value += prev_val
            }
        }
        if value > input {
            return value.to_string();
        }
        values.push((coords, value));
    }
    unreachable!();
}

fn is_adjacent(coord1: (i32, i32), coord2: (i32, i32)) -> bool {
    (coord1.0 - coord2.0).abs() <= 1 &&
    (coord1.1 - coord2.1).abs() <= 1
}

fn coords(idx: i32) -> (i32, i32) {
    let shell = which_shell(idx);
    let edge_len = 2*shell+1;
    let shell_max = edge_len*edge_len;
    let mut corner = shell_max;
    if idx >= corner - (edge_len - 1) {
        return (shell-(corner-idx), -shell);
    }
    corner = corner - (edge_len - 1);
    if idx >= corner - (edge_len - 1) {
        return (-shell, (corner-idx)-shell)
    }
    corner = corner - (edge_len - 1);
    if idx >= corner - (edge_len - 1) {
        return ((corner-idx)-shell, shell);
    }
    corner = corner - (edge_len - 1);
    (shell, shell-(corner-idx))
}


fn which_shell(idx: i32) -> i32 {
    for base in 0.. {
        if idx <= (2*base+1)*(2*base+1) {
            return base;
        }
    }
    return -1;
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert_eq!(coords(1), (0, 0));
        assert_eq!(coords(12), (2, 1));
        assert_eq!(coords(23), (0, -2));
        assert_eq!(solve1("1"), "0");
        assert_eq!(solve1("12"), "3");
        assert_eq!(solve1("23"), "2");
        assert_eq!(solve1("1024"), "31");
    }
    
    #[test]
    fn test2() {
        assert_eq!(solve2("1"), "2");
        assert_eq!(solve2("2"), "4");
        assert_eq!(solve2("3"), "4");
        assert_eq!(solve2("4"), "5");
        assert_eq!(solve2("20"), "23");
        assert_eq!(solve2("100"), "122");
    }
}