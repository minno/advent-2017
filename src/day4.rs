use std::collections::HashSet;

pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn common(input: &str, check: fn(&str) -> bool) -> String {
    input.lines().filter(|s| check(s)).count().to_string()
}

fn solve1(input: &str) -> String {
    common(input, is_valid)
}

fn is_valid(phrase: &str) -> bool {
    let mut words = HashSet::new();
    for word in phrase.split_whitespace() {
        if !words.insert(word) {
            return false;
        }
    }
    true
}

fn solve2(input: &str) -> String {
    common(input, is_valid_anagram)
}

fn is_valid_anagram(phrase: &str) -> bool {
    let mut words = HashSet::new();
    for word in phrase.split_whitespace() {
        let mut sorted_bytes = word.to_string().into_bytes();
        sorted_bytes.sort();
        if !words.insert(sorted_bytes) {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert!(is_valid("aa bb cc dd ee"));
        assert!(!is_valid("aa bb cc dd aa"));
        assert!(is_valid("aa bb cc dd aaa"));
    }
    
    #[test]
    fn test2() {
        assert!(is_valid_anagram("abcde fghij"));
        assert!(!is_valid_anagram("abcde xyz ecdab"));
        assert!(is_valid_anagram("a ab abc abd abf abj"));
        assert!(is_valid_anagram("iiii oiii ooii oooi oooo"));
        assert!(!is_valid_anagram("oiii ioii iioi iiio"));
    }
}