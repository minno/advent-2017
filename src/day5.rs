pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn common(input: &str, incr_op: fn(&mut i32)) -> String {
    let mut offsets = input.split_whitespace()
        .filter_map(|num| str::parse::<i32>(num).ok())
        .collect::<Vec<_>>();
    let mut cursor = 0;
    let mut steps = 0;
    while cursor >= 0 && cursor < offsets.len() as i32 {
        let new_pos = cursor + offsets[cursor as usize];
        incr_op(&mut offsets[cursor as usize]);
        cursor = new_pos;
        steps += 1;
    }
    steps.to_string()
}

fn solve1(input: &str) -> String {
    common(input, |offset| *offset += 1)
}

fn solve2(input: &str) -> String {
    common(input, |offset| 
        if *offset >= 3 {
            *offset -= 1;
        } else {
            *offset += 1;
        }
    )
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert_eq!(solve1("0\n3\n0\n1\n-3\n"), "5");
    }
    
    #[test]
    fn test2() {
        assert_eq!(solve2("0\n3\n0\n1\n-3\n"), "10");
    }
}