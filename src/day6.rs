use std::collections::HashMap;

pub fn solve(input: &str) -> String {
    format!("{}\n\n{}", solve1(input), solve2(input))
}

fn solve1(input: &str) -> String {
    common(input, true)
}

fn solve2(input: &str) -> String {
    common(input, false)
}

fn common(input: &str, include_tail: bool) -> String {
    let mut banks = input.split_whitespace()
        .filter_map(|num| str::parse::<i32>(num).ok())
        .collect::<Vec<_>>();
    let mut count = 0;
    let mut history = HashMap::new();
    history.insert(banks.clone(), count);
    loop {
        let idx = max_idx(&banks);
        distribute_from(&mut banks, idx);
        count += 1;
        if let Some(prev_count) = history.insert(banks.clone(), count) {
            return (if include_tail {
                count
            } else {
                count - prev_count
            }).to_string()
        }
    }
}

fn max_idx(banks: &[i32]) -> usize {
    banks.iter().cloned().enumerate()
        .rev().max_by_key(|&(_, value)| value).unwrap().0
}

fn distribute_from(banks: &mut [i32], mut idx: usize) {
    let mut val = banks[idx];
    banks[idx] = 0;
    while val > 0 {
        idx += 1;
        idx %= banks.len();
        banks[idx] += 1;
        val -= 1;
    }
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test1() {
        assert_eq!(solve1("0\t2\t7\t0\n"), "5");
    }
    
    #[test]
    fn test2() {
        assert_eq!(solve2("0\t2\t7\t0\n"), "4");
    }
}