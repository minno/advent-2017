use std::collections::HashMap;

mod parser {
    use super::{Tree, TreeBuilder};
    use nom::{alpha, digit};
    
    named!(child_list<&str, Vec<String>>, do_parse!(
        tag_s!(" -> ")
        >> names: separated_list_complete!(tag_s!(", "), alpha)
        >> (names.into_iter().map(str::to_string).collect())
    ));
    
    named!(parse_node<&str, (&str, i32, Vec<String>)>, do_parse!(
        name: alpha
        >> tag_s!(" (")
        >> weight: digit
        >> tag_s!(")")
        >> children: opt!(complete!(child_list))
        >> ((name, weight.parse().unwrap(), children.unwrap_or(vec![])))
    ));
    
    pub fn parse(data: &str) -> Option<Tree> {
        let mut tree = TreeBuilder::new();
        for line in data.lines() {
            parse_node(line).map(|(_, data)| {
                let (name, weight, children) = data;
                tree.add_node(name, weight, children);
            }).unwrap();
        }
        tree.build().ok()
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct Tree {
    nodes: HashMap<String, Node>,
}

impl Tree {
    pub fn root_name(&self) -> &str {
        for node in self.nodes.values() {
            if node.parent.is_none() {
                return &node.name;
            }
        }
        panic!("Cyclic tree.")
    }
    
    pub fn weight(&self, node_name: &str) -> i32 {
        let node = &self.nodes[node_name];
        node.weight + node.children.iter().map(|node| self.weight(node)).sum::<i32>()
    }
    
    pub fn get(&self, node_name: &str) -> &Node {
        &self.nodes[node_name]
    }
}

fn get_corrected_weight(tree: &Tree) -> i32 {
    fn get_weight_and_outlier<'a>(weights: &[(&'a str, i32)]) -> (i32, Option<&'a str>) {
        assert!(weights.len() >= 3);
        // Assuming that there is only one outlier
        let normal = if weights[0].1 == weights[1].1 {
            weights[0].1
        } else {
            weights[2].1
        };
        (normal, weights.iter()
            .find(|&&(_, weight)| weight != normal)
            .map(|&(name, _)| name))
    }
    fn get_corrected_weight_recur(tree: &Tree, node_name: &str, correct_weight: i32) -> i32 {
        let node = tree.get(node_name);
        let child_weights = node.children.iter()
            .map(|name| (&**name, tree.weight(name)))
            .collect::<Vec<_>>();
        let (normal, outlier) = get_weight_and_outlier(&child_weights);
        if let Some(name) = outlier {
            get_corrected_weight_recur(tree, name, normal)
        } else {
            node.weight + (correct_weight - tree.weight(node_name))
        }
    }
    get_corrected_weight_recur(tree, tree.root_name(), 0)
}

#[derive(PartialEq, Eq, Debug)]
pub struct Node {
    pub name: String,
    pub weight: i32,
    pub children: Vec<String>,
    pub parent: Option<String>,
}

pub struct TreeBuilder {
    inner: Tree,
}

impl TreeBuilder {
    pub fn new() -> Self {
        TreeBuilder { 
            inner: Tree {
                nodes: HashMap::new()
            }
        }
    }
    
    pub fn add_node(&mut self, name: &str, weight: i32, children: Vec<String>) {
        self.inner.nodes.insert(name.to_string(), Node { 
            name: name.to_string(), 
            weight, children,
            parent: None });
    }
    
    pub fn build(mut self) -> Result<Tree, Self> {
        if !self.is_valid() {
            Err(self)
        } else {
            self.fill_parents();
            Ok(self.inner)
        }
    }
    
    fn is_valid(&self) -> bool {
        for node in self.inner.nodes.values() {
            for child in &node.children {
                if !self.inner.nodes.contains_key(child) {
                    return false;
                }
            }
        }
        true
    }
    
    fn fill_parents(&mut self) {
        let mut parent_map = HashMap::new();
        for node in self.inner.nodes.values() {
            for child in &node.children {
                parent_map.insert(child.clone(), node.name.clone());
            }
        }
        
        for (child, parent) in &parent_map {
            self.inner.nodes.get_mut(child).map(|node| {
                node.parent = Some(parent.clone());
            });
        }
    }
}

pub fn solve(input: &str) -> String {
    let tree = parser::parse(input).unwrap();
    format!("{}\n\n{}", tree.root_name(), get_corrected_weight(&tree))
}

#[cfg(test)]
mod test {
    use super::*;
    
    fn get_example() -> Tree {
        let mut builder = TreeBuilder::new();
        builder.add_node("pbga", 66, vec![]);
        builder.add_node("xhth", 57, vec![]);
        builder.add_node("ebii", 61, vec![]);
        builder.add_node("havc", 66, vec![]);
        builder.add_node("ktlj", 57, vec![]);
        builder.add_node("fwft", 72, "ktlj cntj xhth"
            .split_whitespace()
            .map(str::to_string)
            .collect());
        builder.add_node("qoyq", 66, vec![]);
        builder.add_node("padx", 45, "pbga havc qoyq"
            .split_whitespace()
            .map(str::to_string)
            .collect());
        builder.add_node("tknk", 41, "ugml padx fwft"
            .split_whitespace()
            .map(str::to_string)
            .collect());
        builder.add_node("jptl", 61, vec![]);
        builder.add_node("ugml", 68, "gyxo ebii jptl"
            .split_whitespace()
            .map(str::to_string)
            .collect());
        builder.add_node("gyxo", 61, vec![]);
        builder.add_node("cntj", 57, vec![]);
        builder.build().ok().unwrap()
    }
    
    #[test]
    fn test_parser() {
        let tree = get_example();
        let parsed_tree = parser::parse(r#"pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"#).unwrap();
        assert_eq!(tree, parsed_tree);
    }
    
    #[test]
    fn test1() {
        let tree = get_example();
        assert_eq!(tree.root_name(), "tknk");
    }
    
    #[test]
    fn test2() {
        let tree = get_example();
        assert_eq!(get_corrected_weight(&tree), 60);
    }
}