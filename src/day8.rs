use std::collections::HashMap;
use std::i32;

mod parser {
    use super::{Program, Instruction, Condition, RelOp};
    use nom::{alpha, digit};
    
    named!(integer<&str, i32>, do_parse!(
        sign: opt!(tag_s!("-"))
        >> num: digit
        >> (num.parse::<i32>().unwrap() * if sign.is_some() { -1 } else { 1 })
    ));
    
    named!(comparison<&str, Condition>, do_parse!(
        tag_s!("if ")
        >> reg: alpha
        >> tag_s!(" ")
        >> op: take_until_s!(" ")
        >> tag_s!(" ")
        >> num: integer
        >> (Condition {
            register: reg.into(),
            op: match op {
                "==" => RelOp::Eq,
                ">=" => RelOp::GtE,
                "<=" => RelOp::LtE,
                "!=" => RelOp::NEq,
                ">" => RelOp::Gt,
                "<" => RelOp::Lt,
                _ => panic!("Unexpected comparison: {}", op),
            },
            constant: num
        })
    ));
    
    named!(incr<&str, i32>, do_parse!(
        action: alt!(tag_s!("inc") | tag_s!("dec"))
        >> tag_s!(" ")
        >> amt: integer
        >> (amt * if action == "inc" { 1 } else { -1 })
    ));
    
    named!(parse_instruction<&str, Instruction>, do_parse!(
        reg: alpha
        >> tag_s!(" ")
        >> incr: incr
        >> tag_s!(" ")
        >> comp: comparison
        >> (Instruction {
            register: reg.into(),
            incr,
            condition: comp
        })
    ));
    
    pub fn parse(data: &str) -> Program {
        let instructions = data.lines().map(|line| {
            parse_instruction(line).unwrap().1
        }).collect();
        Program::new(instructions)
    }
}

pub struct Program {
    instructions: Vec<Instruction>,
    registers: Registers,
}

impl Program {
    pub fn new(instructions: Vec<Instruction>) -> Self {
        Program {
            instructions,
            registers: Registers { 
                inner: HashMap::new(),
                highest_value: i32::MIN 
            },
        }
    }
    
    pub fn run(&mut self) {
        for instruction in &self.instructions {
            self.registers.execute(&instruction)
        }
    }
    
    pub fn registers(&self) -> &Registers {
        &self.registers
    }
}

pub struct Registers {
    inner: HashMap<String, i32>,
    highest_value: i32
}

impl Registers {
    fn get(&self, register: &str) -> i32 {
        *self.inner.get(register).unwrap_or(&0)
    }
    
    fn incr(&mut self, register: &str, incr: i32) {
        let reg_ref = self.inner.entry(register.into()).or_insert(0);
        *reg_ref += incr;
        if *reg_ref > self.highest_value {
            self.highest_value = *reg_ref;
        }
    }
    
    fn eval_condition(&self, condition: &Condition) -> bool {
        use self::RelOp::*;
        let x1 = self.get(&condition.register);
        let x2 = condition.constant;
        match condition.op {
            Lt => x1 < x2,
            Gt => x1 > x2,
            LtE => x1 <= x2,
            GtE => x1 >= x2,
            Eq => x1 == x2,
            NEq => x1 != x2,
        }
    }
    
    fn execute(&mut self, instruction: &Instruction) {
        // NLL SVP
        let cond_passes = self.eval_condition(&instruction.condition);
        if cond_passes {
            self.incr(&instruction.register, instruction.incr)
        }
    }
    
    fn largest_value(&self) -> i32 {
        *self.inner.values().max().unwrap_or(&0)
    }
    
    fn largest_ever(&self) -> i32 {
        self.highest_value
    }
}

pub struct Instruction {
    pub register: String,
    pub incr: i32,
    pub condition: Condition,
}

pub struct Condition {
    pub register: String,
    pub op: RelOp,
    pub constant: i32,
}

pub enum RelOp {
    Gt,
    GtE,
    Lt,
    LtE,
    Eq,
    NEq
}

pub fn solve(input: &str) -> String {
    let mut program = parser::parse(input);
    program.run();
    format!("{}\n\n{}", 
        program.registers().largest_value(), 
        program.registers().largest_ever())
}

#[cfg(test)]
mod test {
    use super::*;
    
    fn get_example() -> Program {
        parser::parse(r#"b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"#)
    }
    
    #[test]
    fn test1() {
        let mut program = get_example();
        program.run();
        assert_eq!(program.registers().largest_value(), 1);
    }
    
    #[test]
    fn test2() {
        let mut program = get_example();
        program.run();
        assert_eq!(program.registers().largest_ever(), 10);
    }
}