mod parser {
    
    named!(canceled_char(&str) -> &str, preceded!(
        char!('!'),
        take_s!(1)
    ));
    
    named!(garbage_contents(&str) -> i32, fold_many0!(
        alt!(
            value!("", canceled_char)
            | take_until_either!("!>")
        ),
        0,
        |acc, new: &str| acc + new.len() as i32
    ));
    
    named!(garbage(&str) -> i32, delimited!(
        char!('<'),
        garbage_contents,
        char!('>')
    ));
    
    named!(group_content_element(&str) -> (i32, i32, i32), do_parse!(
        opt!(char!(','))
        >> val: alt!(
            map!(garbage, |val| (0, 0, val))
            | group
        )
        >> (val)
    ));
    
    named!(group_contents(&str) -> (i32, i32, i32), do_parse!(
        values: fold_many0!(
            group_content_element,
            (0, 0, 0),
            |acc: (i32, i32, i32), new: (i32, i32, i32)| 
                (acc.0 + new.0, acc.1 + new.1, acc.2 + new.2)
        )
        >> (
            (values.0 + values.1 + 1, values.1 + 1, values.2)
        )
    ));
    
    // Returns (a, b, c)
    // a: the total score of that group
    // b: the number of groups in that group, including itself
    // c: the number of garbage characters in that group
    named!(group(&str) -> (i32, i32, i32), delimited!(
        char!('{'),
        group_contents,
        char!('}')
    ));
    
    pub fn group_data(data: &str) -> (i32, i32) {
        let (_, (score, _, garbage_amt)) = group(data).ok().unwrap();
        (score, garbage_amt)
    }
}

pub fn solve(input: &str) -> String {
    let (score, garbage_amt) = parser::group_data(input);
    format!("{}\n\n{}", score, garbage_amt)
}

#[cfg(test)]
mod test {
    use super::*;
    use super::parser::*;
    
    #[test]
    fn test1() {
        fn group_score(input: &str) -> i32 {
            group_data(input).0
        }
        assert_eq!(group_score("{}"), 1);
        assert_eq!(group_score("{{{}}}"), 6);
        assert_eq!(group_score("{{},{}}"), 5);
        assert_eq!(group_score("{{{},{},{{}}}}"), 16);
        assert_eq!(group_score("{<a>,<a>,<a>,<a>}"), 1);
        assert_eq!(group_score("{{<ab>},{<ab>},{<ab>},{<ab>}}"), 9);
        assert_eq!(group_score("{{<!!>},{<!!>},{<!!>},{<!!>}}"), 9);
        assert_eq!(group_score("{{<a!>},{<a!>},{<a!>},{<ab>}}"), 3);
    }
    
    #[test]
    fn test2() {
        fn garbage(input: &str) -> i32 {
            group_data(input).1
        }
        assert_eq!(garbage("{<>}"), 0);
        assert_eq!(garbage("{<random characters>}"), 17);
        assert_eq!(garbage("{<<<<>}"), 3);
        assert_eq!(garbage("{<{!>}>}"), 2);
        assert_eq!(garbage("{<!!>}"), 0);
        assert_eq!(garbage("{<!!!>>}"), 0);
        assert_eq!(garbage("{<{o\"i!a,<{i<a>}"), 10);
    }
}