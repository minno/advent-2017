static FUNCTIONS: [fn(&str) -> String; 26] = [
    dummy,
    ::day1::solve,
    ::day2::solve,
    ::day3::solve,
    ::day4::solve,
    ::day5::solve,
    ::day6::solve,
    ::day7::solve,
    ::day8::solve,
    ::day9::solve,
    ::day10::solve,
    ::day11::solve,
    ::day12::solve,
    ::day13::solve,
    ::day14::solve,
    ::day15::solve,
    ::day16::solve,
    ::day17::solve,
    ::day18::solve,
    ::day19::solve,
    ::day20::solve,
    ::day21::solve,
    ::day22::solve,
    ::day23::solve,
    ::day24::solve,
    ::day25::solve,
];

/// Provides the answer for the given day's programming problem.
///
/// Return value must be explicitly deallocated.
///
/// # Safety
/// 
/// Requires that input_ptr be a pointer to a valid String.
#[no_mangle]
pub unsafe fn get_answer(day: i32, input_ptr: *mut String) -> *mut String {
    let output = Box::new(match day {
        0...25 => FUNCTIONS[day as usize](&*input_ptr),
        _ => format!("Error: expected day number, received \"{}\"", day),
    });
    
    Box::into_raw(output)
}

fn dummy(input: &str) -> String {
    input.split(',').collect()
}

/// Allocates a new String on the heap, with space for `len` bytes.
///
/// # Safety
///
/// The string's data must be completely filled by valid utf-8 bytes before
/// being used in safe code.
#[no_mangle]
pub unsafe extern "C" fn string_create(len: i32) -> *mut String {
    let mut s = Box::new(String::with_capacity(len as usize));
    s.as_mut_vec().set_len(len as usize);
    Box::into_raw(s)
}

/// Deallocates the provided String.
///
/// # Safety
///
/// Requires that the `ptr` was allocated in Rust.
#[no_mangle]
pub unsafe extern "C" fn string_dealloc(ptr: *mut String) {
    let _ = Box::from_raw(ptr);
}

/// Retrieves a pointer to the string's data.
///
/// # Safety
///
/// `ptr` must be a valid String. All modifications through this pointer must
/// preserve String's invariant, that it is totally filled with valid utf-8.
#[no_mangle]
pub unsafe extern "C" fn string_data(ptr: *mut String) -> *mut u8 {
    (*ptr).as_bytes_mut().as_mut_ptr()
}

/// Retrieves the string's length, in bytes.
///
/// # Safety
///
/// `ptr` must be a valid String.
#[no_mangle]
pub unsafe extern "C" fn string_len(ptr: *mut String) -> i32 {
    (*ptr).len() as i32
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_dummy() {
        assert_eq!(dummy("a,b,c"), "abc");
    }
    
    #[test]
    fn test_wrapper() {
        unsafe {
            let input = "a,b,c";
            let s = string_create(input.len() as i32);
            let str_data = string_data(s);
            for i in 0..(string_len(s) as usize) {
                *str_data.offset(i as isize) = input.as_bytes()[i];
            }
            assert_eq!(*s, input);
            let o = get_answer(0, s);
            assert_eq!(*o, "abc");
            string_dealloc(s);
            string_dealloc(o);
        }
    }
}